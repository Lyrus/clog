/*
	@File: clog.h
	@Date: 2019/05/26
	@Author: Lyrus
	@Description: 
*/

#ifndef _CLOG_H_
#define _CLOG_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef enum _clog_level {
	CLOG_DEBUG = 0,
	CLOG_INFO = 1,
	CLOG_WARN = 2,
	CLOG_ERROR = 3,
	CLOG_FATAL = 4
}clog_level;

int clog_init(const char *conf_file);
void clog_set_lvl(clog_level lvl);
void clog_close();
void clog_vlog(clog_level lvl, const char *fmt, ...);

#define clog_debug(fmt, ...)\
	clog_vlog(CLOG_DEBUG, __FUNCTION__, __LINE__, __VA_ARGS__);

#define clog_info(fmt, ...)\
	clog_vlog(CLOG_INFO,  __FUNCTION__, __LINE__, __VA_ARGS__);

#define clog_warn(fmt, ...)\
	clog_vlog(CLOG_WARN,  __FUNCTION__, __LINE__, __VA_ARGS__);

#define clog_error(fmt, ...)\
	clog_vlog(CLOG_ERROR,  __FUNCTION__, __LINE__, __VA_ARGS__);

#define clog_fatal(fmt, ...)\
	clog_vlog(CLOG_FATAL,  __FUNCTION__, __LINE__, __VA_ARGS__);

#ifdef __cplusplus
}
#endif

#endif



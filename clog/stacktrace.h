/*
	@File: stacktrace.h
	@Date: 2019/06/03
	@Author: Lyrus
	@Description: 
*/

#ifndef _CLOG_STACK_TRACE_H_
#define _CLOG_STACK_TRACE_H_


#ifdef WIN32
#include <io.h>
#include <direct.h>
#include <Windows.h>
#include <DbgHelp.h>
#include "StrSafe.h"
#pragma comment(lib, "Dbghelp.lib")
#else
#include <unistd.h>
#include <pthread.h>
#include <sys/stat.h>
#include <execinfo.h>
#endif

#ifdef WIN32
#define PROC_HANDLE HANDLE
#define snprintf _snprintf
#define vsnprintf _vsnprintf
#else
#define PROC_HANDLE void *
#endif

typedef struct clog_st_s
{
	PROC_HANDLE curr_proc;
	char *st_line;
}clog_st_t;

int clog_get_curr_proc_handle(clog_st_t *st);
void clog_write_stacktrace_info(FILE *fp, clog_st_t *st);

#endif

/*
	@File: conf_parser.c
	@Date: 2019/05/27
	@Author: Lyrus
	@Description: 
*/

#include "clog.h"
#include "conf_parser.h"
#include "iniparser.h"
#include "dictionary.h"

const char *levels[] = {"debug", "info", "warn", "error", "fatal", NULL};

static int clog_conv_lvl(char *lvl)
{
	int i = 0;
	for (;levels[i] != NULL; ++i)
	{
		if(strcmp(levels[i], lvl) == 0) return i;
	}

	return 3;
}

int clog_conf_parse(const char *conf_file, clog_conf_t *conf)
{
	dictionary *ini = NULL;
	FILE *fp = NULL;
	char *logdir = NULL;
	char *logname = NULL;
	char *filterlvl = NULL;

	if (!conf_file || !conf) return -1;

	ini = iniparser_load(conf_file);
	if (!ini)
	{
		return -1;
	}

	fp = fopen(conf_file, "r");
	if (!fp)
	{
		return -1;
	}

	conf->enable = iniparser_getint(ini, CLOG_CONF_ENABLE, CLOG_CONF_DEF_ENABLE);
	logdir = iniparser_getstring(ini, CLOG_CONF_LOGDIR, CLOG_CONF_DEF_LOGDIR);
	logname = iniparser_getstring(ini, CLOG_CONF_LOGNAME, CLOG_CONF_DEF_LOGNAME);
	strncpy(conf->logdir, logdir, MAX_FILE_PATH - 1);
	strncpy(conf->logname, logname, MAX_FILENAME_LEN - 1);
	filterlvl = iniparser_getstring(ini, CLOG_CONF_FILTERLVL, CLOG_CONF_DEF_FILTERLVL);
	conf->filterlvl = clog_conv_lvl(filterlvl);
	conf->maxlogsize = 1024;
	conf->colorop = iniparser_getint(ini, CLOG_CONF_COLOROP, CLOG_CONF_DEF_COLOROP);
	conf->stacktrace = iniparser_getint(ini, CLOG_CONF_STACKTRACE, CLOG_CONF_DEF_STACKTRACE);

	iniparser_freedict(ini);
	if (fp)
	{
		fclose(fp);
		fp = NULL;
	}

	return 0;
}

int clog_get_conf(clog_conf_t *conf)
{
	if(!conf)
	{

	}

	return 0;
}

void clog_set_conf(clog_conf_t *conf)
{
	
}

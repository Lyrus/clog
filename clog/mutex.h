/*
	@File: mutex.h
	@Date: 2019/05/30
	@Author: Lyrus
	@Description: 
*/

#ifndef _CLOG_MUTEX_H_
#define _CLOG_MUTEX_H_

#ifdef WIN32
#include <Windows.h>
#else
#include <pthread.h>
#endif

#ifdef WIN32
#define CLOG_MUTEX CRITICAL_SECTION
#else
#define CLOG_MUTEX pthread_mutex_t
#endif

void clog_init_mutex(CLOG_MUTEX *mtx);
void clog_lock(CLOG_MUTEX *mtx);
void clog_unlock(CLOG_MUTEX *mtx);
void clog_destroy_mutex(CLOG_MUTEX *mtx);

#endif

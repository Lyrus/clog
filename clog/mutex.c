/*
	@File: mutex.c
	@Date: 2019/05/30
	@Author: Lyrus
	@Description: 
*/

#include "mutex.h"

void clog_init_mutex(CLOG_MUTEX *mtx)
{
#ifdef WIN32
	InitializeCriticalSection(mtx);
#else
	pthread_mutex_init(mtx, NULL);
#endif
}

void clog_lock(CLOG_MUTEX *mtx)
{
#ifdef WIN32
	EnterCriticalSection(mtx);
#else
	pthread_mutex_lock(mtx);
#endif
}

void clog_unlock(CLOG_MUTEX *mtx)
{
#ifdef WIN32
	LeaveCriticalSection(mtx);
#else
	pthread_mutex_unlock(mtx);
#endif
}

void clog_destroy_mutex(CLOG_MUTEX *mtx)
{
#ifdef WIN32
	DeleteCriticalSection(mtx);
#else
	pthread_mutex_destroy(mtx);
#endif
}

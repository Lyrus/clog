/*
	@File: conf_parser.h
	@Date: 2019/05/27
	@Author: Lyrus
	@Description: 
*/

#ifndef _CLOG_CONF_PARSER_H_
#define _CLOG_CONF_PARSER_H_

#include <stdio.h>
#include <stdint.h>
#include "mutex.h"

#define MAX_FILE_PATH 260
#define MAX_FILENAME_LEN 26
typedef struct clog_conf_s
{
	int enable;
	int init;
	char logdir[MAX_FILE_PATH];
	char logname[MAX_FILENAME_LEN];
	int filterlvl;
	int maxlogsize;
	int colorop;
	int stacktrace;
	CLOG_MUTEX mutex;
	FILE *fp;
}clog_conf_t;

#define CLOG_CONF_SECTION "clog"
#define CLOG_CONF_ENABLE "clog:enable"
#define CLOG_CONF_LOGDIR "clog:logdir"
#define CLOG_CONF_LOGNAME "clog:logname"
#define CLOG_CONF_FILTERLVL "clog:filterlvl"
#define CLOG_CONF_MAXLOGSIZE "clog:maxlogsize"
#define CLOG_CONF_COLOROP "clog:colorop"
#define CLOG_CONF_STACKTRACE "clog:stacktrace"

#define CLOG_CONF_DEF_ENABLE 0
#define CLOG_CONF_DEF_LOGDIR "."
#define CLOG_CONF_DEF_LOGNAME "clog.log"
#define CLOG_CONF_DEF_FILTERLVL "error"
#define CLOG_CONF_DEF_MAXLOGSIZE "1M"
#define CLOG_CONF_DEF_COLOROP 1
#define CLOG_CONF_DEF_STACKTRACE 0

int clog_conf_parse(const char *conf_file, clog_conf_t *conf);
int clog_get_conf(clog_conf_t *conf);
void clog_set_conf(clog_conf_t *conf);

#endif

